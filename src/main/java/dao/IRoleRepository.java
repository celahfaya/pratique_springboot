package dao;

import java.util.stream.Stream;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.essoustore.moejaa.model.Role;

/**
 * @author Admin
 *
 */
public interface IRoleRepository extends JpaRepository<Role, Long> {

    /**
     * @param roleName
     * @return
     */
    Role findByRoleName(String roleName);

    /**
     * @return
     */
    @Query("select role from Role role")
    Stream<Role> getAllRolesStream();

}
