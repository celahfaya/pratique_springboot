package service;

import java.util.Collection;

import com.essoustore.moejaa.model.User;

/**
 * @author Admin
 *
 */
public interface IUserService {

    /**
     * @return
     */
    Collection<User> getAllUser();

    /**
     * @param id
     * @return
     */
    User getUserById(Long id);
    
    /**
     * @param login
     * @return
     */
    User findByLogin(String login);

    /**
     * @param user
     * @return
     */
    User saveOrUpdateUser(User user);

    /**
     * @param id
     */
    void deleteUser(Long id);
}
