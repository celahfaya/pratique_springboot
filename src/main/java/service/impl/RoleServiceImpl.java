package service.impl;

import java.util.Collection;
import java.util.stream.Stream;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.essoustore.moejaa.model.Role;

import dao.IRoleRepository;
import service.IRoleService;

/**
 * @author Admin
 *
 */
@Service(value = "roleService")
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleRepository roleRepository;

    /**
     *
     */
    @Override
    public Role findByRoleName(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }

    /**
     *
     */
    @Override
    public Collection<Role> getAllRoles() {
        return IteratorUtils.toList(roleRepository.findAll().iterator());
    }

    /**
     * 
     */
    @Override
    public Stream<Role> getAllRolesStream() {
        return roleRepository.getAllRolesStream();
    }

}
