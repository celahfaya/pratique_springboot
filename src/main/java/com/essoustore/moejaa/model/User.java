package com.essoustore.moejaa.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.essoustore.moejaa.dto.UserDTO;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "UTILISATEUR")
@XmlRootElement(name = "user")
public class User implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID", updatable = false, nullable = false)
    private Long              id;

    @Column(name = "LOGIN", unique = true, insertable = true, updatable = true, nullable = false)
    private String            login;

    @Column(name = "USER_PASSWORD", insertable = true, updatable = true, nullable = false)
    private String            password;

    @Column(name = "USER_ACTIVE", insertable = true, updatable = true, nullable = false)
    private Integer           active;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLE", joinColumns = @JoinColumn(name = "USER_ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role>         roles            = new HashSet<>();

    public User() {
        super();
    }

    public User(final String login, final String password, final Integer active) {
        this.login = login;
        this.password = password;
        this.active = active;
    }

    public User(final Long id, final String login) {
        this.id = id;
        this.login = login;
    }

    public User(String login) {
        this.login = login;
    }

    public User(UserDTO userDTO) {
        this.setId(userDTO.getId());
        this.setLogin(userDTO.getLogin());
        this.setPassword(userDTO.getPassword());
        //}

        //public User(UserRegistrationForm userRegistrationForm) {
        //  this.setLogin(userRegistrationForm.getLogin());
        //this.setPassword(userRegistrationForm.getPassword());
    }

    public User(final Long id, final String login, final String password, final Integer active) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.active = active;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    @XmlElement
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    @XmlElement
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the passeword
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param passeword the passeword to set
     */
    @XmlElement
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    @XmlElement
    public void setActive(final Integer active) {
        this.active = active;
    }

    /**
     * @return the roles
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    @XmlElement
    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", login=" + login + ", passeword=XXXXXXX,active=" + active + ", roles=" + roles + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (active == null) {
            if (other.active != null)
                return false;
        } else if (!active.equals(other.active))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (id.equals(other.id))
            return false;
        if (login == null) {
            if (other.login != null)
                return false;
        } else if (!login.equals(other.login))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (roles == null) {
            if (other.roles != null)
                return false;
        } else if (!roles.equals(other.roles))
            return false;
        return true;

    }

}
