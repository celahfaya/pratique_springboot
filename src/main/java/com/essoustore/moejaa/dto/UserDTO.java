package com.essoustore.moejaa.dto;

import java.io.Serializable;

/**
 * @author Admin
 *
 */
public class UserDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4302125081854765220L;

    private Long              id;
    private String            login;
    private String            password;
    private String            userType;

    public UserDTO() {

    }

    /**
     * @param login
     * @param password
     */
    public UserDTO(final String login, final String password) {
        this.login = login;
        this.password = password;
    }

    /**
     * @param login
     * @param password
     * @param userType
     */
    public UserDTO(final String login, final String password, final String userType) {
        this.login = login;
        this.password = password;
        this.userType = userType;
    }

    /**
     * @param id
     * @param login
     */
    public UserDTO(final Long id, final String login) {
        this.id = id;
        this.login = login;
    }

    /**
     * @param id
     * @param login
     * @param password
     * @param userType
     */
    public UserDTO(final Long id, final String login, final String password, final String userType) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.userType = userType;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(final String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return String.format("[id=%s,mail=%s,userType=%s]", id, login, userType);
    }

}
